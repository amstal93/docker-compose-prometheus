# docker-compose-prometheus

This docker-compose configuration configures a Prometheus server.

Use
[ansible-role-docker-compose](https://gitlab.com/naturalis/lib/ansible/ansible-role-docker-compose)
to start and manage your docker-compose application using Ansible.
